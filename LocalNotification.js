/**
	Phonegap LocalNotification Plugin

  iOS/Android  (this file)
    Unified by Brian Olore 2012
	  MIT Licensed

  iOS  (original)
	  Copyright (c) Greg Allen 2011
	  Updates Drew Dahlman 2012
	  MIT Licensed

  Android (original)
    Created by Daniel van 't Oever 2012 MIT Licensed
**/

var LocalNotification = (function (cordova) {

  function LocalNotification() {};

	LocalNotification.add = function(options) {
/*
FOR REFERENCE:
    var defaults = {
      id:           0,      //Android String ("0")
      date:         false,  //Android defaults to new Date()
      message:      '',     //same
      hasAction:    true,   //iOS only
      action:       'View', //iOS only
      badge:        0,      //iOS only
			sound:        '',     //iOS only
			background:   '',     //iOS only
			foreground:   '',     //iOS only
      ticker:       '',     //Android only
      repeatDaily:  false,  //Android only
    };
*/

    var defaultsAndroid = {
      id:           "0",        
      date:         new Date(),
      message:      '',
      ticker:       '',     
      repeatDaily:  false, 
    };
    a = new Date();
    a.setSeconds(a.getSeconds() + 12);
    //alert(a);
    var defaultsIOS = {
      id:           0,
      date:         a,
      message:      'This is a Test by Nils',
      hasAction:    true,
      action:       'View',
      badge:        2,
			sound:        '',
			background:   '2',
			foreground:   '',
    };
    //alert('new Notification 1');
    //alert(defaultsIOS[date]);
    var defaults = defaultsIOS;
                         
    if (device.platform.toLowerCase() == 'android') {
      defaults = defaultsAndroid;
    }
                        // alert('new Notification 2');
   /* for (var key in defaults) {
        alert('new Notification 2a');
      if (typeof options[key] !== "undefined") {
                         alert('new Notification 2b');
        defaults[key] = options[key];
      }
        alert('new Notification 2c');
    }*/

		if (typeof defaults.date == 'object') {
			defaults.date = Math.round(defaults.date.getTime()/1000);
		}
                         //alert('new Notification 3');
    if (device.platform.toLowerCase() == 'android') {
      defaults.date = (options.date.getMonth()) + "/" + (options.date.getDate()) + "/"
					+ (options.date.getFullYear()) + "/" + (options.date.getHours()) + "/"
					+ (options.date.getMinutes());
    }
    
    //alert('new Notification 4');
    cordova.exec(null,null,"LocalNotification","addNotification",[defaults]);
	};
                         /**
                          * Add reminder for Challanges
                          *
                          * @param text, startDateTime
                          *        Text that sould be Pushed, Date of Challange start, duration
                          */
    LocalNotification.addChallangeReminder = function(text, duration, first_Alert_Date) {
                         //dateTime = new Date();
                         dateTime = first_Alert_Date;
                         
                         for (var i = 0; i < duration; i++){
                         
                         dateTime.setSeconds(dateTime.getSeconds() + 86400);
                         
                         
                         var defaultsIOS = {
                            id:           0,
                            date:         dateTime,
                            message:      text,
                            hasAction:    false,
                            action:       'View',
                            badge:        0,
                            sound:        '',
                            background:   '',
                            foreground:   '',
                         };
                         
                         var defaults = defaultsIOS;
                         
                         
                         if (typeof defaults.date == 'object') {
                         defaults.date = Math.round(defaults.date.getTime()/1000);
                         }
                         if (device.platform.toLowerCase() == 'android') {
                         defaults.date = (options.date.getMonth()) + "/" + (options.date.getDate()) + "/"
                         + (options.date.getFullYear()) + "/" + (options.date.getHours()) + "/"
                         + (options.date.getMinutes());
                         }
                         
                         cordova.exec(null,null,"LocalNotification","addNotification",[defaults]);
        }
                         console.log('New Push Reminders added!');
    };

                         
	/**
	 * Cancel an existing notification using its original ID.
	 * 
	 * @param id
	 *            The ID that was used when creating the notification using the
	 *            'add' method.
	 */
	LocalNotification.cancel = function(id) {
    if (device.platform.toLowerCase() == 'android') {
      id = new Array({ id : id });
    }
		cordova.exec(null, null, "LocalNotification", "cancelNotification", id);
	};
	
	/**
	 * Cancel all notifications that were created by your application.
	 */
	LocalNotification.cancelAll = function(id) {
    cordova.exec(null, null, "LocalNotification", "cancelAllNotifications", []);
    
  };

  cordova.addConstructor(function () {
    if (!window.plugins) {
      window.plugins = {};
    }
    window.plugins.localNotification = LocalNotification;
  });

  return LocalNotification;

})(window.cordova || window.Cordova || window.PhoneGap);
