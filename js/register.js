function register(){
	
	var username = $('#username').val();
	var password = $('#password').val();
	var passwordRepeat = $('#passwordRepeat').val();
	var email = $('#email').val();	
	
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/register.php?jsoncallback=?';
					
		$.post(path,{
					'username':username, 
					'password':password, 
					'email':email
					}, 
			function(result){					
				if (result['status'] == 'register ok') {
					$('#Register #username').val("");
					$('#Register #password').val("");
					$('#Register #passwordRepeat').val("");
					$('#Register #email').val("");
					
					window.localStorage.setItem('username', username);
					window.localStorage.setItem('email', email);
					window.localStorage.setItem('userID', result['userID']);
					
					$('#OwnChallengeList #ownChallengeListview').empty();
					$('#OwnChallengeList #notStartedChallengeListview').empty();
					$.mobile.changePage("index.html#OwnChallengeList", { transition: "slide"});
					
					ownChallengeList();
				} 
				else if(result['status'] == 'Benutzername und/oder E-Mail-Adresse vergeben') {
					
					myAlert("Benutzername und/oder E-Mail-Adresse vergeben");
					
				}
			}, "json");
}
