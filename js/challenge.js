global_obj.challenges = new Array();

/**
 * Konstruktor
 * i_item ist ein Objekt wie es aus der Datenbank kommt.
 * Erzeugte Challenge-Instanzen werden im global_obj.challenges Array() gesichert und sind über die "id" anzusprechen
 * 
 * @param i_item
 * @returns
 */
function Challenge(i_item){
	this.id = i_item.startedChallengeID;
	this.challengeID = i_item.challengeID;
	this.challengeName = i_item.challengeName;
	this.categoryID = i_item.categoryID;
	this.challengeDesc = i_item.challengeDesc;
	this.challengeUsage = i_item.usage;
	this.startDate = castPhpDate(i_item.startDate);
	this.endDate = castPhpDate(i_item.endDate);
	this.counter = i_item.counter;
	this.lastModified = i_item.lastModified;
	this.userID = i_item.userID;
	
	// Not sure, if I really need this
	this.isStarted = i_item.isStarted;
	this.isFinished = i_item.isFinished;
	
	global_obj.challenges[this.id] = this;
	
	return this;
}

/**
 * Methode zum Darstellen einer Challenge in der OhwnCallengeList
 */
Challenge.prototype.paintPreview = function(){
	var html = null;
	
	if ((this.getWhetherStarted()) && (this.endDate > new Date())){
		html = '<li><a href="#" class="ownChallengeDetailItem" onclick="ownChallengeView('+ this.id +')">'
		+ this.challengeName + this.getPreviewProgressbar() +'</a></li>';
		
		$('#OwnChallengeList #ownChallengeListview').append(html);
	} else {
		html = '<li><a href="#" class="ownChallengeDetailItem" onclick="ownChallengeView('+ this.id +')">'
		+ this.challengeName +' (Startet am '+ this.startDate.toDateString() +')</a></li>';
		
		$('#OwnChallengeList #notStartedChallengeListview').append(html);
	}
	
	return html;
}

/**
 * Bereitet die Daten einer Challenge für das Erstellen einer Progressbar auf
 * wird unter anderem von .paintPreview() und ownChallengeView() aufgerufen
 */
Challenge.prototype.getPreviewProgressbar = function(){
	
	var durations = calcDuration(this.startDate, this.endDate);
	var not_done = durations.duration_now - this.counter;
	
	var html = getProgressbar(this.counter, not_done, durations.duration_total);
	return html;
}

/**
 * Gibt zurück, ob die Challenge gestartet ist
 * wird von .paintPreview verwendet
 */
Challenge.prototype.getWhetherStarted = function(){
	var todayNight = new Date();
	todayNight.setHours(0);
	todayNight.setMinutes(0);
	todayNight.setSeconds(0);
	
	if (this.startDate <= todayNight){
		return true;
	} else {
		return false;
	}
}