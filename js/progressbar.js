/**
 * Generiert das HTML für eine Progressbar
 * @param i_done
 * @param i_not_done
 * @param i_total
 * @returns {String}
 */
function getProgressbar(i_done, i_not_done, i_total){
	var percentage_done = i_done / i_total * 100;
	var percentage_not_done = i_not_done / i_total * 100;
	
	var html = '</br><span class="progressbar">'
		+ '<span class="progressbar_done" style="width:' + percentage_done + '%"></span>'
		+ '<span class="progressbar_not_done" style="width:' + percentage_not_done + '%"></span>'
		+ '</span>';
	
	if (percentage_done + percentage_not_done > 100){
		//Should not happen in my opinion. Depends on the Use-Case. Do we have Use-Cases? I guess not... 
		html = "";
	}
	
	return html;
}

/**
 * Formatiert Progress-Bars. Muss immer im Anschluss an das Einfügen von Progressbars vorgenommen werden
 * Getrennt vom getProgressbar() um unnötig häufige Ausführungen zu verhindern
 */
function formatProgressbars(){
	$('.progressbar').css({'width':'100%', 'display':'block', 'min-height':'20px', 'border-radius':'10px','background-color':'#333333'});
	$('.progressbar_done').css({'display':'block', 'float':'left', 'min-height':'20px', 'border-radius':'8px','background-color':'#00ee55'});
	$('.progressbar_not_done').css({'display':'block', 'float':'right', 'min-height':'20px', 'border-radius':'8px','background-color':'#ee0000'});
}

/**
 * Hilfsfunktion für das Erzeugen der nötigen Aufrufparameter von getProgressbar()
 * Wandelt Start- und Enddatum in eine Dauer um
 * @param i_start_date
 * @param i_end_date
 * @returns {___result0}
 */
function calcDuration(i_start_date, i_end_date){
	var duration_total = i_end_date - i_start_date;
	duration_total = duration_total/1000/60/60/24;
	
	var today = new Date();
	today.setHours(0);
	today.setMinutes(0);
	today.setSeconds(0);
	today.setMilliseconds(0);
	today.setDate(today.getDate() + 1);
	
	var duration_now = today - i_start_date;
	duration_now = duration_now/1000/60/60/24;
	
	var result = new Array();
	result['duration_total'] = duration_total;
	result['duration_now'] = duration_now;
	
	return result;
}