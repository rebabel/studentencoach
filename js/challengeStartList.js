/**
 * Prüft erst, ob alle Daten richtig eingegeben wurden
 * Holt dann die Freunde, die eingeladen werden können und zeigt sie mit Slide-Buttons an
 * @returns {Boolean}
 */
function challengeStartList (){
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/challengeStartList.php?jsoncallback=?';
	var userID = window.localStorage["userID"];
	
	var startDate = $('#ChallengeDetail #datepickerStartDate').val();
	var challengeID = $('#ChallengeDetail #challengeID').text();
	var duration = $('#ChallengeDetail #duration').val();
	
	$('#ChallengeDetail #startDateError').hide();
	
	if ((startDate == "") || (duration == "") || isNaN(duration)) {
		$('#ChallengeDetail #startDateError').text('Bitte fülle alle Felder korrekt aus.');
		$('#ChallengeDetail #startDateError').css("display", "block");
		return false;
	}
	
	startDate = castPhpDate(startDate);
	startDate.setHours(23);
	startDate.setMinutes(59);
	
	if (startDate < new Date()){
		$('#ChallengeDetail #startDateError').text('Das Startdatum kann nicht in der Vergangenheit liegen.');
		$('#ChallengeDetail #startDateError').css("display", "block");
		return false;
	}
	
	
	$.mobile.changePage('index.html#ChallengeStart', { transition: "slide"});

	$.post(path, {'userID': userID}, 
		function(result) {		
 			// list all challenges
			var listItem = null;

			$('#ChallengeStart #challengeStartListview').empty();			
            $.each(result, function(index, item){
				listItem = '<li data-role="fieldcontain" id="userID-'+ item['userID'] +'"><span style="width:50%;vertical-align:top;display:inline-block">'+ item['username'] + '</span>'
					+ '<div style="text-align:right;display:inline-block;width:50%"><select data-role="slider" style="width: 300px">'
                	+ '<option value="off">Nein</option>'
                	+ '<option value="on">Ja</option>'
                	+ '</select></div></li>';
				
				//onclick="challengeStartAddFriend('+ item['userID'] +')"
				//<a href="#" class="challengeStartListItem"></a>
				//data-icon="plus"
				 $('#ChallengeStart #challengeStartListview').append(listItem);
            });		
            
			
            $('#ChallengeStart #challengeStartListview').listview('refresh').trigger('create')
            
			// challengeID 
			$('#ChallengeStart #challengeID').text(challengeID);
			$('#ChallengeStart #participantsIDs').text("");
			// startDate
			$('#ChallengeStart #startDate').text(startDate);
			$('#ChallengeGo #startDate').text(startDate);			

		}, "json"
	);
}
