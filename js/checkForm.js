/**
 * Prüft Registrierdaten
 */
function checkForm() {

  //var password = $('#password').val();
  var passwordRepeat = $('#passwordRepeat').val();
  
  var haserror = false; 
  
  var username = $.mobile.activePage.find('#username').val();
  var password = $.mobile.activePage.find('#password').val();
  var email = $.mobile.activePage.find('#email').val();
  
  $('#Fehlermeldung').empty();
  	$('#Register #username').empty();
	$('#Register #password').empty();
	$('#Register #passwordRepeat').empty();
	$('#Register #email').empty();
	
  if (username=="" || username.length<5 ){
  	
    $('#Fehlermeldung').append('<p>Bitte geben Sie einen Benutzernamen mit mindestens 5 Zeichen an.</p>');
    $('#Fehlermeldung').css('display', 'block');
    haserror = true;    
    
  }
   
  if (password.length<5){
  	
  	$('#Fehlermeldung').append('<p>Das gew�hlte Passwort muss mindestens 5 Zeichen haben.</p>');
  	$('#Fehlermeldung').css('display', 'block');
    haserror = true;
      
  }
		  
  if (password != passwordRepeat){
	
    $('#Fehlermeldung').append('<p>Die Passw�rter stimmen nicht �berein.</p>');
    $('#Fehlermeldung').css('display', 'block');
    haserror = true;
		 
  }
		  
  if (!validEmail(email)) {
	
	$('#Fehlermeldung').append('<p>Bitte geben Sie eine korrekte Email - Adresse an.</p>');
	$('#Fehlermeldung').css('display', 'block');
    haserror = true;
		
  }
		  
		 
  if (haserror == false){
  	
  	register();
  	
  }
  	

}

function validEmail(email) {

  var strReg = "^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$";

  var regex = new RegExp(strReg);

  return(regex.test(email));

}

