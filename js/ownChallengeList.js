function ownChallengeList (){
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/ownChallengeList.php?jsoncallback=?';
	var userID = window.localStorage["userID"];
	
	$.post(path, 
		{'userID': userID},
		function(result) {		
 			// list all challenges
			var listItem = null;

			var content = $('#OwnChallengeList #ownChallengeListview').html();
			
			$('#OwnChallengeList #ownChallengeListview').empty();
			$('#OwnChallengeList #notStartedChallengeListview').empty();
            
           var i = 0;
           $.each(result, function(index, item){
				if (item['isFinished'] == '0'){
					var cur_challenge = new Challenge(item);
					listItem = cur_challenge.paintPreview();
                  i++;
				}
			});
			
           if(i==0){
                var html='<div style="background-color: #FFFFFF; border-radius: 5px; padding:10px;"><strong>Neu hier?</strong><br />'
                	+ 'Schaue gleich, ob deine <a href="#FriendList" data-transition="slide" onclick="friendList()">Freunde</a> schon da sind. <br />'
                	+ 'Danach kannst du zuerst einmal ein <a href="#p_goals" data-transition="slide" onclick="ownGoalList()">großes Ziel</a> anlegen. '
                	+ 'Jetzt bist du auch schon bereit dich einer <a href="#ChallengeList" data-transition="slide" onclick="challengeList())">bestehenden Challenge</a> zu stellen '
                	+ 'oder dir <a href="#" onclick="challengeAddView()">neue Challanges</a> zu setzen und dich selbst heraus zu fordern!</div>';
                $('#OwnChallengeList #ownChallengeListview').append(html);
           }
			formatProgressbars();
			
			$('#OwnChallengeList #ownChallengeListview').listview('refresh');
			$('#OwnChallengeList #notStartedChallengeListview').listview('refresh');

		}, "json"
	);
}
