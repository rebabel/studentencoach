/**
 * 
 * ############### Instructions #####################
 * 
 * Über den Aufruf von new Notification(parameters) wird eine Notification in das Array im global_obj gesichert (Parameter siehe Konstruktor)
 * Alle dortigen Notifications werden in der Dropdownliste angezeigt
 * Mit der Funktion showButtonNumbers() kannst du Nummern oben rechts an den Buttons erzeugen.
 * Du musst keine Notifications aus dem Array löschen, das passiert immer dann, wenn der Callback der Notification ausgelöst wurde.
 * Falls du hier dann noch was an die Datenbank zurücksenden musst, dann kannst du den Code in der Methode Notification.prototype.evalCallback()
 * ergänzen.
 * Besser aber, du bringst es direkt in der Callback-Funktion unter.
 * Falls es "nur" vorgefertigte Callbacks gibt, bringst du diese am besten im Konstuktor unter, evlt sogar abhängig vom Typ.
 * Aktuell akzeptierte Typen sind "friend" und "challenge" (eben für die beiden Buttons).
 * 
 * ##################################################
 * 
 */


global_obj.notification = new Array();


/**
 * Konstruktor für Notification, werden automatisch in global_obj.notification abgelegt, über 'nid' + id zugreifbar
 * @param i_id
 * @param i_type
 * @param i_message
 * @param i_button_text
 * @param i_callback
 * @returns
 */
function Notification(i_id, i_type, i_message, i_callback){
	this.id = 'nid' + i_id;
	this.type = i_type;
	this.message = i_message;
	this.callback = i_callback;
	
	global_obj.notification[this.id] = this;
}

/**
 * Initiierung der Buttons in der Header-Leiste (nur Design)
 */
$(document).ready(function(){
	
	$('.buttonbar_notification').html('<div id="button_friend_request" class="button_notification button_friend_request"></div>'
									+ '<div id="button_challenge_request" class="button_notification button_challenge_request"></div>');
	
	$('.buttonbar_notification').css({'display':'inline-block', 'width':'60%', 'margin':'0.2em 20% 0.2em', 'min-height':'45px','text-align':'center'});
	$('.button_notification').css({'height': '20px', 'width':'20px', 'background-image':'url("css/images/icons-36-white-pack.png")', 'display':'inline-block'});
	$('.button_notification:hover').css( 'background-color','#ff0000');
	
	$('.button_friend_request').css({'background-position':'-1438px -36px', 'margin-right':'30px', 'height':'40px', 'width':'40px'}).click(function(){showNotificationList();});
	$('.button_challenge_request').css({'background-position':'-862px 2px','height':'40px', 'width':'40px'}).click(function(){showNotificationList();});
	
	// Testen mit: 
//	new Notification(1, 'friend', 'Hallo Welt',  'Give it to me, baby!', function(){myalert('Hallo Welt.');});
//	new Notification(2, 'friend','Funktion 2', 'Do', function(){});
//	showButtonNumbers();
//	
	
});

/**
 * Holt die Freundschaftsanfragen vom Backend
 * wird aufgerufen in navigation.js im "pagechange"-Handler
 */
function getFriendRequests (){
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/friendRequestList.php?jsoncallback=?';
	var userID = window.localStorage["userID"];
	
	$.post(path, {
		'userID' : userID
		}, function(result) {
			$.each(result, function(index, item) {
				
				new Notification('friend' + item['userID'], 'friend', '<b>' + item['username'] + '</b>' + ' hat dir eine Freundschaftsanfrage gesendet.', function(i_what){
					
					var response_path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/friendRequestReply.php?jsoncallback=?';	
					
					$.post(response_path, {
							'myUserID' : window.localStorage["userID"],
							'friendsUserID' : item['userID'],
							'memo' : i_what
						}, function(result) {
							if (i_what == 1){
								myAlert('Freundschaft best�tigt!');
							} else {
								myAlert('Freundschaft abgelehnt.');
							}
							
						}, "json");
				});
			});
			
			showButtonNumbers();
	}, "json");

}

/**
 * Holt die Challenge-Anfragen vom Backend
 * wird aufgerufen in navigation.js im "pagechange"-Handler
 */
function getChallengeRequests (){
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/challengeRequest.php?jsoncallback=?';
	var userID = window.localStorage["userID"];
	
	$.post(path, {
		'userID' : userID
		}, function(result) {
			$.each(result, function(index, item) {
				new Notification('challenge' + item['startedChallengeID'], 'challenge', '<b>' + item['inviterUsername'] + '</b> hat dich zu einer Challenge eingeladen.', function(i_what){
					
					if (i_what != 'i'){
						var response_path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/handleChallengeRequest.php?jsoncallback=?';	
						
						$.post(response_path, {
								'userID' : window.localStorage["userID"],
								'startedChallengeID' : item['startedChallengeID'],
								'isAccepted' : i_what
							}, function(result) {
									if (i_what == 1){
										myAlert('Du hast die Challenge akzeptiert!');
									} else {
										myAlert('Du hast die Challenge abgelehnt.')
									}
									
									ownChallengeList();
							}, "json");
					} else {
						 ownChallengeView(item['startedChallengeID']);
					}
					
				});
			});
			

			showButtonNumbers();
	}, "json");

}

/**
 * Erzeugt die Ausgabe einer Notification in der Notification-List
 * @param id
 */
Notification.prototype.print = function(id){
	var html;
	
	if (this.type == 'friend'){
		html = '<div class="notification_message"><p style="text-align:left; margin:0px; font-size:0.9em">' + this.message + '</p>'
		+ '<div data-role="controlgroup" data-type="horizontal" data-mini="true">'
		+ '<a href="#" style="width:100px" data-role="button" data-icon="check" onclick="global_obj.notification[\'' + this.id + '\'].evalCallback(\'1\')" data-iconpos="notext"></a>'
		+ '<a href="#" style="width:100px" data-role="button" data-icon="delete" data-iconpos="left" onclick="global_obj.notification[\'' + this.id + '\'].evalCallback(\'2\')" data-iconpos="notext"></a>'
		+ '</div>';
	} else if (this.type == 'challenge'){
		html = '<div class="notification_message"><p style="text-align:left; margin:0px; font-size:0.9em">' + this.message + '</p>'
		+ '<div data-role="controlgroup" data-type="horizontal" data-mini="true">'
		+ '<button data-icon="info" data-iconpos="left" onclick="global_obj.notification[\'' + this.id + '\'].evalCallback(\'i\')">Info</button>'
		+ '<a href="#" style="width:100px" data-role="button" data-icon="check" onclick="global_obj.notification[\'' + this.id + '\'].evalCallback(\'1\')" data-iconpos="notext"></a>'
		+ '<a href="#" style="width:100px" data-role="button" data-icon="delete" data-iconpos="left" onclick="global_obj.notification[\'' + this.id + '\'].evalCallback(\'2\')" data-iconpos="notext"></a>'
		+ '</div>';
	}
					
	$('#' + id ).append(html).trigger('create');
};

/**
 * Wird auslgelöst, wenn Button einer Notification ausgelöst wird
 * (siehe callback im Konstruktor)
 */
Notification.prototype.evalCallback = function(i_what){
	var cur_callback;

	cur_callback = this.callback;

	delete global_obj.notification[this.id];
	hideNotificationList();
	
	cur_callback(i_what);
	
};

/**
 * Generiert HTML f�r Notification List, holt Notifications
 */
function showNotificationList(){
	if ($('#notification_list').html() == null){
		var cur_page = $.mobile.activePage;
		var cur_content = cur_page.children(2)[1];
		//if (cur_content.attributes['data-role'] == "content"){
		$(cur_content).prepend('<div id="notification_list" style="width:80%;background:-webkit-linear-gradient(#5393C5, #6FACD5)"></div>');
		$('#notification_list').css('position', 'relative');
		$('#notification_list').css('margin', 'auto');
		$('#notification_list').css('top', '-15px');
		$('#notification_list').css('border-radius', '0px 0px 20px 20px');
		$('#notification_list').css('text-align', 'center');
		$('#notification_list').hide();
		//}	

		
		for (key in global_obj.notification){
			var cur_notification = global_obj.notification[key];
			cur_notification.print('notification_list');
		}
		
		$('.notification_message').css({'padding':'5px 15px 0px 15px', 'color':'#ffffff', 'text-shadow':'0 1px 0 #3373a5', 'border-style':'solid', 'border-width':'0px 0px 2px 0px','background': '-webkit-linear-gradient(#5393C5, #6FACD5)'});
		
		$('#notification_list').append('<div style="width: 162px; margin:auto; text-align:center; background-image:url(img/icon_up.png); height:25px" onclick="hideNotificationList()"></div>');
		
		
		$('#notification_list').slideDown();
	} else {
		hideNotificationList();
	}	
}

/**
 * L�st Notification List wieder verschwinden 
 */
function hideNotificationList(){
	$('#notification_list').slideUp(function(){$('#notification_list').remove();});
	showButtonNumbers();
};


/**
 * Offene Notificatoins anzeigen
 * wird bei jedem Seitenwechsel ausgelöst (navigation.js "pagechange"-Hanlder)
 */
function showButtonNumbers(){
	
	var number_friend = 0; 
	var number_challenge = 0;
	
	for (key in global_obj.notification){
		var cur_notification = global_obj.notification[key];
		
		switch(cur_notification.type){
		case 'friend': number_friend++; break;
		case 'challenge': number_challenge++; break;
		}
	}
	
	showRequestAmountOnButton('.button_friend_request', number_friend);
	showRequestAmountOnButton('.button_challenge_request', number_challenge);
}


/**
 * Zeigt die Anzahl der noch offenen Requests auf einem Button an
 * wird von showButtonNumbers() aufgerufen
 * @param id
 * @param amount
 */
function showRequestAmountOnButton(id, amount){
	
	var element = $.mobile.activePage.find(id);
	var subid = id.substring(8,14);
	
	$('.nid' + subid).remove();
	
	try{
		element.prepend('<div class="nid' + subid + '" style="display:block;width:20px;height:20px;border-radius:10px;background-color:#ff0000">' + amount + '</div>');
		$('.nid' + subid).css('position', 'relative');
		$('.nid' + subid).css('top', '-2px');
		$('.nid' + subid).css('left', '12px');
		$('.nid' + subid).css('z-index', '1000');
		$('.nid' + subid).css('display', 'inline-block');
		$('.nid' + subid).css('text-align', 'center');
                if(amount == 0){
                    $('.nid' + subid).css('opacity', '0');
                }
                
	} catch(e){
		// sollte nie vorkommen
	}
}