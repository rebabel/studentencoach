// getUrlParam
function getUrlParam( query ) {
	query = query.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var expr = "[\\?&]"+query+"=([^&#]*)";
	var regex = new RegExp( expr );
	var results = regex.exec( window.location.href );
	if ( results !== null ) {
		return results[1];
	} else {
		return false;
	}
}

// script including
function includeJs(url) {
  document.write(
                  unescape('%3Cscript%20type%3D%22text%2Fjavascript%22%20src%3D%22')+
                  url+
                  unescape('%22%3E%3C%2Fscript%3E')
                );
}

/**
 * Hilfsfunktion, die ein PHP-Datum in ein Javascript Date-Object umwandelt
 * @param i_date
 * @returns {Date}
 */
function castPhpDate(i_date){
	var year = i_date.substr(0,4);
	var month = i_date.substr(5,2) - 1;
	var days = i_date.substr(8,2);
	
	var date = new Date(year, month, days);
	return date;
}