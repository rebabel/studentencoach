function challengeRatingAdd (){
	var comment = $('#ChallengeFinishedDetail #comment').val();
	var rating = null;
	var userID = window.localStorage["userID"];
	
	rating = $.mobile.activePage.find('#rating_count').html();
	
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/challengeRatingAdd.php?jsoncallback=?';
	
	$.getJSON(path,{'userID': userID, 
					'rating': rating, 
					'comment': comment,
					'startedChallengeID': global_obj.trophy.current.challengeID,
					},
		function(result){		
			if (result['status'] == true) {
				challengeFinishedView(global_obj.trophy.current.challengeID);
			}else{
				myAlert("Hinzufügen fehlgeschlagen");
			}
		}
	);	
}
