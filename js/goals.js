/**
 * 
 * ##################### Instructions ######################
 * 
 * Alle Goals werden als Objekte in global_obj.goals[] gespeichert. Dazu muss
 * lediglich der Konstruktor für ein Goal aufgerufen werden. 
 * 
 * ######################################################### 
 * 
 */

global_obj.goals = new Array();

/**
 * Konstruktor
 * Erzeugte Instanzen werden im global_obj.goals gesichert und sind über die "id" anzusprechen
 * 
 * @param i_id
 * @param i_title
 * @param i_url
 * @returns
 */
function Goal(data) {
	this.id = data.g_id
	this.title = data.g_title;
	this.url = data.URL;

	global_obj.goals[data.g_id] = this;
	return this;
}
/**
 * Löschen eines Ziels nachdem "Erreicht" bestätigt wurde
 *
 */
function DelGoal(id, i_delete_span) {
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/goalDel.php?jsoncallback=?';
    //alert('del: '+id);
    console.log('deleting goal: '+id);
    $.getJSON(path, {
              'g_id' : id
              }, function(result) {
                    if (result['status'] == true) {
                    $(i_delete_span).parent().remove();
                    delete global_obj.goals[id];
                    myAlert('Herzlichen Glückwunsch! Am besten setzt du dir nun ein neues Ziel.');
              } else {
                    myAlert('Arbeite weiter dran, es lohnt sich!');
              }            
        });
}

/**
 * Getter-Methode zum holen der URL im CSS-Format
 */
Goal.prototype.getUrl = function() {
	return "url(" + this.url + ")";
}

/**
 * Darstellen eines Goals in der Seite
 */
Goal.prototype.displayGoal = function() {

	var content = $(document).find('#c_goals');
	content
			.append('<div class="goal" onclick="global_obj.goals[\''
					+ this.id
					+ '\'].onGoalSelect(this, event)"><span style="float:left;font-size:25pt;color:#000000">'
					+ this.title + '</span><div>');
	
	var cur_goal = content.children().last();
	cur_goal.css('background-image', this.getUrl());
	cur_goal.css('background-color', '#555555');
	cur_goal.css('background-size', '100%');
	cur_goal.css('background-position', '-50%');
	cur_goal.css('width', '100%');
	cur_goal.css('min-height', '200px');
	
}

/**
 * Goal öffnen, wenn es angeklickt wird
 * (Background löschen und <image /> einfügen)
 * entsprechende onclick-Handler einfügen
 */
Goal.prototype.onGoalSelect = function(i_div, event) {

	var cur_goal = $(i_div);
	//cur_goal.removeAttr('onclick');
	cur_goal
			.append('<span class="goal_delete_button" style="float:right" onclick="global_obj.goals[\''
					+ this.id
					+ '\'].onDelete(this, event)""><img style="width:40px;height:40px" src="img/check.png" /></span>');
	cur_goal.append('<image src="' + this.url
			+ '" onclick="global_obj.goals[\'' + this.id
			+ '\'].onGoalImageSelect(this, event)"/>');

	cur_goal.css('background-image', 'none');
	var cur_img = cur_goal.children().last();
	cur_img.css('width', cur_img.parent().width());
}

/**
 * Goal schließen, wenn das Bild angeklickt wird
 * (<image /> löschen und Background einfügen)
 * entsprechende onclick-Handler einfügen
 */
Goal.prototype.onGoalImageSelect = function(i_img, event) {
	if (i_img.localName == 'img') {
		event.stopPropagation();
		
		var cur_goal = $(i_img).parent();
		$(i_img).remove();
		
		cur_goal.find('.goal_delete_button').remove();
		cur_goal.css('background-image', this.getUrl());
		
		cur_goal.attr('onclick', 'global_obj.goals[\'' + cur_this.id
				+ '\'].onGoalSelect(this)');		
	}
}

/**
 * Abfrage auslösen, ob Ziel erreicht wird bei Klick auf den Check-Haken
 */
Goal.prototype.onDelete = function(i_delete_span, e) {
		e.stopPropagation();
	
        var delSpan = i_delete_span;
        var g_id = this.id;
        navigator.notification.confirm(
                                       'Hast du dein Ziel erreicht?',  // message
                                       function(button){if(button==1){DelGoal(g_id, delSpan);}else{}},              // callback to invoke with index of button pressed
                                       'Ziel erreicht',            // title
                                       'Ja,Nein'          // buttonLabels
                                       );
}

/**
 * Lässt alle Goals in global_obj.goals anzeigen
 */
function ownGoalList() {
	$('#c_goals').empty();
	$('#formNewGoalInputUserID').val(window.localStorage["userID"]);

	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/ownGoalList.php?jsoncallback=?';
	var userID = window.localStorage["userID"];

	$.getJSON(path, {
		'userID' : userID
	}, function(result) {
		$('#c_goals').empty();
		global_obj.goals = new Array();

		
		$.each(result, function(index, item) {
			if (item != null){
				global_obj.goals[item['g_id']] = new Goal(item);
			}
		});
		for (key in global_obj.goals) {
			var cur_obj = global_obj.goals[key];
			cur_obj.displayGoal();
		}
        document.getElementById('formNewGoal').reset();

	});
}


/**
 * Überprüft das Formular auf fehlende Eingaben und Dateiformate. Erlaubte Formate sind jpg, jpeg, gif, png.
 */

function goaladd(title, file) {
	
	var error = false;
	
	if (title == "" && file == "") {
	$('#p_goals #formtitleError').text('Bitte einen Titel eingeben!');
	$('#p_goals #formtitleError').css("display", "block");
	$('#p_goals #formfileError').text("Bitte eine Datei auswählen!");
	$('#p_goals #formfileError').css("display", "block");
	error = true;
	return false;
	}
	
	else if (title == "") {
	$('#p_goals #formtitleError').text('Bitte einen Titel eingeben!');
	$('#p_goals #formtitleError').css("display", "block");
	$('#p_goals #formfileError').text("");
	$('#p_goals #formfileError').css("display", "none");
	error = true;
	return false;
	
	}
	else if (file == "") {
	$('#p_goals #formfileError').text("Bitte eine Datei auswählen!");
	$('#p_goals #formfileError').css("display", "block");
	$('#p_goals #formtitleError').text("");
	$('#p_goals #formtitleError').css("display", "none");
	error = true;
	return false;
	}
  else{
	
	var allowed_extensions = new Array('jpg', 'jpeg', 'gif', 'png');
	var extension = file.split('.');
	
	extension = extension[extension.length - 1];
	
	var match = false;
	for (var k in allowed_extensions) {
	if (allowed_extensions[k] == extension) {
    match = true;
    break;
	}
}
		if (match == false) {
		$('#p_goals #formtitleError').text("");
		$('#p_goals #formtitleError').css("display", "none");
		$('#p_goals #formfileError').text("Bitte wähle ein anderes Dateiformat aus!");
		$('#p_goals #formfileError').css("display", "block");
		error = true;
		return false;
		}
		
		else {
		$('#p_goals #formtitleError').text("");
		$('#p_goals #formtitleError').css("display", "none");
		$('#p_goals #formfileError').text("");
		$('#p_goals #formfileError').css("display", "none");
		myAlert ("Ziel gespeichert!");
		error = false;
		$('#newgoal').trigger('collapse');
		
		var new_goal = new Goal({g_id : 'neff', g_title : title, URL : "jquery/css/images/ajax-loader.gif"});
		new_goal.displayGoal();
		setTimeout('ownGoalList()', 10000);
		
		return true;
		}
		

	}

}
