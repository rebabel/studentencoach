/**
 * Holt die Kategorien um sie als Dropdown beim Anlegen neuer Challenges anzuzeigen
 */
function challengeAddView() {
	// category list
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/challengeCategoryList.php?jsoncallback=?';
	
	$.getJSON(path,
		function(result){		
			$.each(result, function(index, item){
				$('#ChallengeAdd #category').append('<option value="'+ item['categoryID'] +'">'+ item['categoryName'] +'</option>');
			});

					
			$.mobile.changePage('index.html#ChallengeAdd', { transition: "slide"});
		}
	);	
	
}