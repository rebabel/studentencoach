function trophyList (){
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/trophyList.php?jsoncallback=?';
	var userID = window.localStorage["userID"];
	
	$.post(path, {'userID': userID},
		function(result) {		
 			// list all challenges
			var listItem = null;
			
			var content = $('#TrophyList #trophyListview').html();
			
			$('#TrophyList #trophyListview').empty();
            $.each(result, function(index, item){
				listItem = '<li><a href="#" class="trophyItem" onclick="challengeFinishedView('+ item['startedChallengeID'] +')">'+ item['challengeName'] +'</a></li>';
				// append and refresh
				if (content == "") {
					$('#TrophyList #trophyListview').append(listItem);
				}else{
					$('#TrophyList #trophyListview').append(listItem);
				}
            });
            
            $('#TrophyList #trophyListview').listview('refresh');
		}, "json"
	);
}
