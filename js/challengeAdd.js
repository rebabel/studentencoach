function challengeAdd (){
	var title = $('#title').val();
	var category = $('#category').val();
	var usage = $('#usageField').val();
	var challengeDescription = $('#challengeDescriptionArea').val();
	
	var error = false;
	
	if (title == "") {		
		$('#ChallengeAdd #titleError').text('Bitte gib einen Namen ein!');
		$('#ChallengeAdd #titleError').css("display", "block");
		error = true;
	}
	if (category == "") {		
		$('#ChallengeAdd #categoryError').text('Bitte wähle eine Kategorie aus!');
		$('#ChallengeAdd #categoryError').css("display", "block");
		error = true;
	}
	if (usage == "") {		
		$('#ChallengeAdd #usageError').text('Bitte gib einen Nutzen ein!');
		$('#ChallengeAdd #usageError').css("display", "block");
		error = true;
	}
	if (challengeDescription == "") {		
		$('#ChallengeAdd #descriptionError').text('Bitte gib eine Beschreibung ein!');
		$('#ChallengeAdd #descriptionError').css("display", "block");
		error = true;
	}
	if (error == true) {
		return false;
	}
	
	
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/challengeAdd.php?jsoncallback=?';
	
	$.getJSON(path,{'title': title, 
					'category': category, 
					'challengeDescription': challengeDescription,
					'usage': usage
					},
		function(result){		
			if (result['status'] == true) {
				$("#ChallengeDetail #challengeTitle").text(result['challengeName']);	
				$("#ChallengeDetail #challengeDescription").text(result['challengeDesc']);
				$("#ChallengeDetail #usage").text(result['usage']);
				$("#ChallengeDetail #challengeID").text(result['challengeID']);
			
				$('#ChallengeAdd #title').val("");
				$('#ChallengeAdd #challengeDescriptionArea').val("");
				$('#ChallengeAdd #usageField').val("");
				
				$.mobile.changePage('index.html#ChallengeDetail', { transition: "slide"});
			}else{
				myAlert("Hinzufügen Failed");
			}
		}
	);	
}
