/**
 * Liest sich die nötigen Daten aus und sendet alle Daten ans Backend um eine Challenge zu starten und gebenenfalls Challenge-Anfragen zu versenden
 */
function challengeGo (){
	getFriendsToAsk();
	
	$('#challenge_go_button').hide();
	$('#challenge_notgo_button').hide();
	
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/challengeGo.php?jsoncallback=?';
	var userID = window.localStorage["userID"];

	var startDate = $(document).find('#datepickerStartDate').val();
	var duration = $(document).find('#ChallengeDetail #duration').val()
	var challengeID = $('#ChallengeStart #challengeID').text();
	var participantsIDs = $('#ChallengeStart #participantsIDs').text();
    var push_start_date = new Date(startDate+'T10:30:00');
	
	$.post(path, {'userID': userID,
				  'startDate': startDate,
				  'challengeID': challengeID,
				  'participantsIDs': participantsIDs,
				  'duration': duration}, 
		function(result) {	
			
			$(document).find('#ChallengeDetail #duration').val('');
			
			//$( "#ChallengeGo" ).dialog( "close" );
			$.mobile.changePage('#OwnChallengeList', {transition : "slide"});		
			ownChallengeList();
			
			$('#challenge_go_button').show();
			$('#challenge_notgo_button').show();
           
			try{
				 LocalNotification.addChallangeReminder('Denke an die Challange \"'+ result['challengeName'] +'\"!', duration, push_start_date);
			} catch(e){
				console.log('Notifications in Browser-Mode not available!');
			}
		}, "json"
	);
}
