/**
 * Prüft, ob User noch angemeldet ist, bei Neustart der App
 * @returns {Boolean}
 */
function checkPreAuth() {

    if(window.localStorage["username"] != undefined && window.localStorage["userID"] != undefined) {
      
		  return true;
		  $.mobile.changePage("index.html#OwnChallengeList", { transition: "slide"});
        
    } else {
	
		return false;
		$.mobile.changePage("index.html#Register", { transition: "slide"});
		
       }
}