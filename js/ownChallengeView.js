function ownChallengeView (startedChallengeID){
	var startedChallengeID = startedChallengeID;
	
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/ownChallengeView.php?jsoncallback=?';
	var userID = window.localStorage["userID"];

	
	$.post(path, {'startedChallengeID': startedChallengeID, 'userID': userID }, 
		function(result) {					
			$("#OwnChallengeDetail #challengeTitle").text(result['challengeName']);	
			$("#OwnChallengeDetail #challengeDescription").text(result['challengeDesc']);
			$("#OwnChallengeDetail #usage").text(result['usage']);
			$("#OwnChallengeDetail #startedChallengeID").text(startedChallengeID);
			
			
			if (result['isStarted'] == '1') {
				$("#OwnChallengeDetail #daysLeft").html('Die Challenge begann am: ' + castPhpDate(result['startDate']).toDateString() + '<br />Du hast noch '+ result['daysLeft'] +' Tage');
			
				if (result['isAccepted'] != 0){
					if (result['todayDone'] == '0') {
						$("#OwnChallengeDetail #doAim").html('<a href="#" data-role="button" onclick="dailyChallengeDone()" data-transition="slide" data-mini="true" data-theme="b">Ich habe mein Tagesziel erf&uumlllt</a>');
					}else{
						$("#OwnChallengeDetail #doAim").text('Du hast dein Tagesziel erreicht!');
					}
				}
			}else{
				$("#OwnChallengeDetail #daysLeft").text("");
				$("#OwnChallengeDetail #doAim").text('Die Challenge beginnt erst am '+ result['startDate'] +'.');
			}
			
			// participants
			$("#OwnChallengeDetail #participants").empty();

			var durations = calcDuration(castPhpDate(result['startDate']), castPhpDate(result['endDate']));
			
			$.each(result['participants'], function(index, item){				
				var not_done = durations.duration_now - item['counter'];			
				var html = getProgressbar(item['counter'], not_done, durations.duration_total);
				
				$("#OwnChallengeDetail #participants").append('<div><p>' + item['username'] + '</p>' + html + '</div>');
			});
			
			formatProgressbars();
			
//			content = $("#OwnChallengeDetail #participants").text();
//			$("#OwnChallengeDetail #participants").text(content.substring(0, content.length - 2));			
			
			$.mobile.changePage('index.html#OwnChallengeDetail', { transition: "slide"});
			$.mobile.activePage.trigger("create");
		}, "json"
	);
	
}
