/**
 * Holt und bereitet Daten für das Anzeigen einer Trophy auf
 * Neben dem Befüllen der Felder wird auf ein Rating geprüft, welches gegebenenfalls angezeigt wird
 * @param startedChallengeID
 */
function challengeFinishedView (startedChallengeID){
	var startedChallengeID = startedChallengeID;
	var userID = window.localStorage["userID"];
	
	global_obj['trophy'] = {};
	global_obj.trophy['current'] = {};
	global_obj.trophy.current['challengeID'] = startedChallengeID;
	
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/challengeFinishedView.php?jsoncallback=?';
				
	$.post(path, {'startedChallengeID': startedChallengeID,
				  'userID': userID}, 
		function(result) {		
			var participants = "";
			$.each(result['participants'], function(index, item) {
				participants = item['username'] + ', ' + participants;
			});				

			$("#ChallengeFinishedDetail #challengeTitle").text(result['challengeName']);	
			$("#ChallengeFinishedDetail #challengeDescription").text(result['challengeDesc']);			
			$("#ChallengeFinishedDetail #challengeStatus").html('Du hast diese Challenge am ' + result['startDate'] +  ' mit '+ participants +' gestartet und am ' + result['endDate'] + ' beendet.<br />Du hast ' + result['score'] + '% erreicht. Glückwunsch!');
			$("#ChallengeFinishedDetail #challengeStartAgain").html('<a href="#" onclick="challengeView('+ result['challengeID'] +')" id="challengeStartAgainButton" data-role="button">Challenge erneut starten</a>');
			
			// refresh button
			$('#challengeStartAgainButton').button();
			
			// rating view or rating form
			// it's not possible to rate a done challenge twice
			if (result['challengeRatingID']) {
				// delete form and add comment view
				$('#ChallengeFinishedDetail form#ratingForm').hide();
				$('#ChallengeFinishedDetail p#challengeRating').show();
				
				$('#ChallengeFinishedDetail p#challengeRating').html('<strong>Deine Bewertung:</strong><br />' + result['comment']);
				
				makeSpanToRating('#rating_challenge_finished', result['rating'], 'readonly');
				
			}else{
				$('#ChallengeFinishedDetail form#ratingForm').show();
				$('#ChallengeFinishedDetail p#challengeRating').hide();
				
				makeSpanToRating('#rating_challenge_finished', result['rating'], 'write');
			}
			
			$.mobile.changePage('index.html#ChallengeFinishedDetail', { transition: "slide"});
		}, "json"
	);
	
}
