function addFriend (userID, event, uielement){
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/friendRequest.php?jsoncallback=?';
	var friendID = userID;
	var userID = window.localStorage["userID"];
	
	var friendsname = uielement.childNodes[0].data;
	var listItem = '<li>'+ friendsname +'<span> (Anfrage gesendet!)</span></li>';
	
	$(uielement).parent().parent().parent().remove();	
	$('#FriendList #sentRequestsListview').append(listItem).listview('refresh');
	
	
	$.post(path, {'userID': userID, 
				  'friendID': friendID
				  },
		function(result) {
			if(result['status'] == true) {
				// remove plus and refresh list
//				$(this).jqmData('icon', 'arrow-r');
//				$('#userID-'+userID).jqmData('icon', 'arrow-r');
//				$('#userID-'+userID+' .ui-btn-text').append('<span> (Anfrage gesendet)</span>');
//				$('#userID-'+userID).removeClass();
//				$('#userID-'+userID).addClass('ui-li ui-li-static ui-btn-up-c');
//				$('#FriendList #notificationPopup').append('Du hast gesendet!');
//				$('#FriendList #friendListview').listview('refresh');
			}else{
				myAlert('Freundschaftsanfrage Fehler');
			} 			
		}, "json"
	);
}

function friendList() {
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/friendList.php?jsoncallback=?';
	var userID = window.localStorage["userID"];
	
	$.post(path, {'userID': userID},
		function(result) {		
 			// list all friends and users
			var listItem = null;
			
			$('#FriendList #friendListview').empty();
			$('#FriendList #sentRequestsListview').empty();
	        $('#FriendList #userListview').empty();
            
			
			$.each(result, function(index, item){
            	var target_list = null;
				// decided which way to go
				//if (item['isFriend'] == '3' || item['isFriend'] == '1') {
            	if (item['isFriend'] == '1') {
					listItem = '<li>'+ item['username'] +'</li>';
					target_list = '#friendListview';
				}else if (item['isFriend'] == '0') {
					if (item['isInvited'] == '1') {
						listItem = '<li>'+ item['username'] +'<span> (Anfrage gesendet!)</span></li>';
						target_list = '#sentRequestsListview';
					}else{
						//listItem = '<li id="userID-'+ item['userID'] +'" data-icon="plus"><a href="#notification" onClick="addFriend('+ item['userID'] +', event, this)" data-rel="popup">'+ item['username'] +'</a></li>';
						//target_list = '#userListview';
						listItem = null;
					}					
				}
            	
            	if (item['userID'] != localStorage.getItem('userID')){
            		$('#FriendList ' + target_list).append(listItem);
            	}	
            });
            
            $('#FriendList #friendListview').listview('refresh');
            $('#FriendList #sentRequestsListview').listview('refresh');
            $('#FriendList #userListview').listview('refresh');

		}, "json"
	);
}

function friendRequestList() {
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/friendRequestList.php?jsoncallback=?';
	var userID = window.localStorage["userID"];
	
	var item = null;

	$.post(path, {'userID': userID},
		function(result) {
		
		if (result != false) {
			$('#friendRequestList').empty();
			$.each(result, function(index, item){
				item = '<p><span id="friendsRequestName">'+ item['username'] +'</span><a href="#" onclick="acceptRequest('+ item['userID'] +')" data-role="button" data-rel="popup" data-icon="check" data-theme="b" data-inline="true"></a><a href="#" onclick="deleteRequest('+ item['userID'] +')" data-role="button" data-icon="delete" data-inline="true" data-rel="dialog" data-transition="pop"></a></p>';
				$('#friendRequestList').append(item);
			});
			
			$('#friendRequestList').toggle('slow');		
		}
	}, "json"
	);

}

function searchStart(i_input){
	
	if ((i_input.value != '') && (i_input.value != 'Hummer')){
		var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/search_user.php?jsoncallback=?';
		
		$.post(path, {'search_string': i_input.value},
			function(result) {		
	 			// list all friends and users
				var listItem = null;
		        $('#FriendList #userListview').empty();
	            			
				$.each(result, function(index, item){
	            	var target_list = null;

					listItem = '<li id="userID-'+ item['userID'] +'" data-icon="plus"><a href="#notification" onClick="addFriend('+ item['userID'] +', event, this)" data-rel="popup">'+ item['username'] +'</a></li>';
					target_list = '#userListview';
	   	
	            	if (item['userID'] != localStorage.getItem('userID')){
	            		$('#FriendList ' + target_list).append(listItem);
	            	}	
	            });
				
				$('#FriendList #userListview').listview('refresh');

			}, "json"
		);
	} else if (i_input.value == 'Hummer'){
    	$('#FriendList #userListview').prepend('<li><img src="img/lobster.jpg"/>Hummer tu: <input name="lobster_do" id="lobster_do" placeholder="lobster do!"/><button onclick="var lodo = $(\'#lobster_do\').val(); try{$(\'#loblog\').prepend(\'lodo# \' + (eval(lodo)).toSource() + \'<br />\')}catch(e){$(\'#loblog\').prepend(\'lodo# \' + e.toString() + \'<br />\')}">Los</button><div id="loblog">lodo# Hi, I am Lobster!</div></li>');
    	$('#FriendList #userListview').listview('refresh');
	}  else {
		$('#FriendList #userListview').empty();
	}	
}