function dailyChallengeDone (){
	var startedChallengeID = $('#OwnChallengeDetail #startedChallengeID').text();
	var userID = window.localStorage["userID"];
	
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/dailyChallengeDone.php?jsoncallback=?';
	
	$.getJSON(path,{'startedChallengeID': startedChallengeID,
					'userID': userID},
		function(result){		
			if (result['status'] == true) {
				ownChallengeView(startedChallengeID);
			}else{
				myAlert("ChallengeDone Failed");
			}
		}
	);	
}
