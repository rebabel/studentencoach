/**
 * Generiert aus einer <span></span> ein Rating mit 5 Sternen und löscht bestehendes
 * rekursive Funktion, die nötiges js-generiert um sich selbst wieder aufzurufen
 * beachte: i_readonly!
 * @param i_span
 * @param i_count
 * @param i_readonly
 */
function makeSpanToRating(i_span, i_count, i_readonly){
	if (i_count == null){
		i_count = 0;
	}
	
	var funct = '';
	
	$(i_span).empty();
	
	for (var i=0;i<i_count;i++){
		var doStar = i + 1;
		
		if (i_readonly != 'readonly'){
			funct = 'makeSpanToRating(\'' + i_span + '\',' + doStar + ',\'write\')';
		}
		
		$(i_span).append('<div class="star"><image src="img/star.png" style="width:50px;height:50px" onclick="' + funct + '"/></div>');
	}
	
	for (var i=i_count;i<5;i++){
		var doStar = i + 1;
		
		if (i_readonly != 'readonly'){
			funct = 'makeSpanToRating(\'' + i_span + '\',' + doStar + ',\'write\')';
		}
		
		$(i_span).append('<div class="star"><image src="img/star_grey.png" style="width:50px;height:50px" onclick="' + funct + '"/></div>');
	}
	
	$(i_span).append('<span id="rating_count" style="display:none">'+ i_count +'</span>')
	$('.star').css({'display':'inline-block'});
}