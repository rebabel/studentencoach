function challengeView (challengeID){
	var challengeID = challengeID;
	
    
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/challengeView.php?jsoncallback=?';
				
	$.post(path, {'challengeID': challengeID }, 
		function(result) {	
			$('#ChallengeDetail #startDateError').hide();
		
			$("#ChallengeDetail #challengeTitle").text(result['challengeName']);	
			$("#ChallengeDetail #challengeDescription").text(result['challengeDesc']);
			$("#ChallengeDetail #usage").text(result['usage']);
			$("#ChallengeDetail #challengeID").text(challengeID);
			
			$.mobile.changePage('index.html#ChallengeDetail', { transition: "slide"});
		}, "json"
	);
	
}
