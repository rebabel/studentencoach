/**
 * Holt sich die Liste aller angelegter Challenges und gibt sie aus
 */
function challengeList (){
	var path = 'http://dhbw-ravensburg-studenten.de/wi/StudentcoachApp/challengeList.php?jsoncallback=?';

	$.post(path, 
		function(result) {		
 			// list all challenges
			var listItem = null;
			
			var content = $('#challengeListview').html();

			$('#challengeListview').empty();			
            $.each(result, function(index, item){
				listItem = '<li><a href="#" class="challengeDetailItem" onclick="challengeView('+ item['challengeID'] +')">'+ item['challengeName'] +'</a></li>';
				if (content == "") {
					$('#challengeListview').append(listItem);
				}else{
					$('#challengeListview').append(listItem);
				}
            });	
            
            $('#challengeListview').listview('refresh');
			
		

		}, "json"
	);
}
