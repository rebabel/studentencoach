/**
 * 
 * ##################### Instructions ######################
 * 
 * Fügt die Navigation jeweils auf die Seite ein, wenn sie geladen wird.
 * So soll da Problem mit den "flaschen" aktiven Buttons behoben werden.
 * 
 * #########################################################
 * 
 */

/**
 * Nachdem Seite geladen wurde werden noch Notifications abgefragt, bzw. die Navbar gerendert
 */
$(document).on("pagechange", function(event) {
	var active_page = $.mobile.activePage[0].id;

	switch (active_page) {
		case 'ChallengeGo':
			break;
		default:
				$.mobile.activePage.find('#nav_elem_' + active_page).addClass(
						'ui-btn-active');
				$.mobile.activePage.find('#navigationbar').parent().navbar();

				try {
					global_obj.notification = new Array();
					getFriendRequests();
					getChallengeRequests();
				} catch (e) {
					// Wenn User noch nicht eingeloggt ist
				}
			}
});


/**
 * Beim Laden der neuen Seite wird die Navigation angefügt
 */
$(document).on("pageshow", function(event){
	var active_page = $.mobile.activePage[0].id;

	switch (active_page) {
	case 'ChallengeGo':
		break;
	default:
		$.mobile.activePage.find('.footer').empty();
		$.mobile.activePage.find('.footer').append('<div><ul id="navigationbar"></ul></div>');

		var cur_navbar = $.mobile.activePage.find('.footer').children(1).children(1)[0];
		
		$(cur_navbar).append('<li><a id="nav_elem_OwnChallengeList" href="#OwnChallengeList" data-transition="slide" onclick="customChangePage(function(){ownChallengeList()})"><image style="width:30px;height:30px" src="img/home.png"/></a></li>');
		$(cur_navbar).append('<li><a id="nav_elem_ChallengeList" href="#ChallengeList" data-transition="slide" onclick="customChangePage(function(){challengeList()})"><image style="width:30px;height:30px" src="img/challenge.png"/></a></li>');
		$(cur_navbar).append('<li><a id="nav_elem_TrophyList" href="#TrophyList" data-transition="slide" onclick="customChangePage(function(){trophyList()})"><image style="width:30px;height:30px" src="img/trophy.png"/></a></li>');
		$(cur_navbar).append('<li><a id="nav_elem_FriendList" href="#FriendList" data-transition="slide" onclick="customChangePage(function(){friendList()})"><image style="width:30px;height:30px" src="img/friend.png"/></a></li>');
		$(cur_navbar).append('<li><a id="nav_elem_p_goals" href="#p_goals" data-transition="slide" onclick="customChangePage(function(){ownGoalList()})"><image style="width:30px;height:30px" src="img/aim.png"/></a></li>');
//		$(cur_navbar).append('<li><a id="nav_elem_ChallengeList" data-icon="grid" href="#ChallengeList" data-transition="slide" onclick="customChangePage(function(){challengeList()})">Challenges</a></li>');
//		$(cur_navbar).append('<li><a id="nav_elem_TrophyList" data-icon="star" href="#TrophyList" data-transition="slide" onclick="customChangePage(function(){trophyList()})">Troph&aumlen</a></li>');
//		$(cur_navbar).append('<li><a id="nav_elem_FriendList" data-icon="person" href="#FriendList" data-transition="slide" onclick="customChangePage(function(){friendList()})">Freunde</a></li>');
//		$(cur_navbar).append('<li><a id="nav_elem_p_aims" data-icon="info" href="#p_aims" data-transition="slide" onclick="customChangePage(function(){displayAllAims()})">Ziele</a></li>');
		
	}
			
});

/**
 * Erweiterter Handler für die Funktionen beim Seitenwechsel.
 * Macht hier gerade nichts, kann aber als Hook-Methode für eventuell nötige Ergänzungen und Prüfungen genutzt werden
 * @param i_function
 */
function customChangePage(i_function){
//	$.mobile.changePage(i_id);
//, { transition: "slide" } 
	var do_following = i_function;
	do_following();
}


/**
 * deprecated
 * wird jetzt in der im "pagechange" Handler gemacht
 */
//function changeActiveState(id){
//	var cur_navb = $.mobile.activePage.find('#navigationbar').children().children(1)[id];
//	$(cur_navb).first().addClass('ui-btn-active');
//
//}
