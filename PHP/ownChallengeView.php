<?php
	// include global
	require_once("global.php");
	
	// assign variables
	$startedChallengeID = intval($_GET['startedChallengeID']);
	$userID = intval($_GET['userID']);
	
	// build sql query
	$sql = "SELECT * FROM started_challenge sc
			INNER JOIN challenge c ON c.challengeID = sc.challengeID
			INNER JOIN challenge_participants cp ON cp.startedChallengeID = sc.startedChallengeID
			WHERE 
				sc.startedChallengeID = ".$startedChallengeID." 
				AND cp.userID = ".$userID;

	// send sql query
	$result = mysql_query($sql);
	
	// fetch data
	$challengeDetails = array();
	$challengeDetails = mysql_fetch_assoc($result);

	$challengeDetails['daysLeft'] = round((strtotime($challengeDetails['endDate']) - strtotime(date('Y-m-d')))/86400, 0);

	if (date('Y-m-d', strtotime($challengeDetails['lastModified'])) == date('Y-m-d')) {
		if ((date('Y-m-d', strtotime($challengeDetails['startDate'])) == date('Y-m-d')) AND $challengeDetails['counter'] == 0) {
			$challengeDetails['todayDone'] = 0;
		}else{
			$challengeDetails['todayDone'] = 1;
		}
	}else{
		$challengeDetails['todayDone'] = 0;
	}	

	if (date('Y-m-d', strtotime($challengeDetails['startDate'])) <= date('Y-m-d')) {
		$challengeDetails['isStarted'] = 1;
	}else{
		$challengeDetails['isStarted'] = 0;
	}
	
	$sql = "SELECT u.userID, u.username, cp.counter FROM challenge_participants cp
			INNER JOIN started_challenge sc ON sc.startedChallengeID = cp.startedChallengeID
			INNER JOIN user u ON u.userID = cp.userID
			WHERE 
				cp.isAccepted = 1
				AND cp.startedChallengeID = ".$startedChallengeID;
	$result = mysql_query($sql);
	while ($row = mysql_fetch_assoc($result)) {
		$challengeDetails['participants'][] = $row;
	}
	
	echo $_GET['jsoncallback'];
	echo '('.json_encode($challengeDetails).');';
	
?>
