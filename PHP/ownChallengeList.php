<?php
	// include global
	require_once("global.php");

	// assign variables
	$userID = intval($_GET['userID']);

	// build sql query
	$sql = "SELECT * FROM challenge c
			INNER JOIN started_challenge sc ON sc.challengeID = c.challengeID
			INNER JOIN challenge_participants cp ON cp.startedChallengeID = sc.startedChallengeID
			WHERE cp.isFinished = 0
			AND cp.isAccepted = 1
			AND cp.userID = ".$userID;
	
	// send sql query
	$result = mysql_query($sql);
	
	$challenges = array();
	$sqlUpdateFinished = "";
	while ($row = mysql_fetch_assoc($result)) {
		$challenges[$row['startedChallengeID']] = $row;
		if (date('Y-m-d', strtotime($row['startDate'])) <= date('Y-m-d')) {
			$challenges[$row['startedChallengeID']]['isStarted'] = "1";
		}else{
			$challenges[$row['startedChallengeID']]['isStarted'] = "0";
		}
		
		if (date('Y-m-d', strtotime($row['endDate'])) <= date('Y-m-d')) {
			$challenges[$row['startedChallengeID']]['isFinished'] = 1;
			$sqlUpdateFinished .= "UPDATE challenge_participants SET isFinished = 1 WHERE startedChallengeID = ".$row['startedChallengeID'].";";
		}
	}
	
	// update is finished
	mysql_query($sqlUpdateFinished);
	
	echo $_GET['jsoncallback'];
	echo '('.json_encode($challenges).');';
?>
