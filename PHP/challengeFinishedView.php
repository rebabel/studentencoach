<?php
	// include global
	require_once("global.php");
	
	// assign variables
	$startedChallengeID = intval($_GET['startedChallengeID']);
	$userID = intval($_GET['userID']);
	
	// build sql query
	$sql = "SELECT * FROM started_challenge sc
			INNER JOIN challenge c ON c.challengeID = sc.challengeID
			INNER JOIN challenge_participants cp ON cp.startedChallengeID = sc.startedChallengeID
			LEFT OUTER JOIN challenge_rating cr ON cr.startedChallengeID = sc.startedChallengeID
			WHERE sc.startedChallengeID = ".$startedChallengeID."
				AND cp.userID = ".$userID;

	// send sql query
	$result = mysql_query($sql);
	
	// fetch data
	$challenge = array();
	$challenge = mysql_fetch_assoc($result);
	
	$challenge['score'] = round(($challenge['counter'] / 0.21) , 0);
	
	$sql = "SELECT u.userID, u.username, cp.counter FROM challenge_participants cp
			INNER JOIN started_challenge sc ON sc.startedChallengeID = cp.startedChallengeID
			INNER JOIN user u ON u.userID = cp.userID
			WHERE 
				cp.isAccepted = 1
				AND cp.startedChallengeID = ".$startedChallengeID;
	$result = mysql_query($sql);
	while ($row = mysql_fetch_assoc($result)) {
		$challenge['participants'][] = $row;
	}
	
	echo $_GET['jsoncallback'];
	echo '('.json_encode($challenge).');';
	
?>
