<?php
	// include global
	require_once("global.php");
	
	// assign variables
	$userID = intval($_GET['userID']);
	$challengeID = intval($_GET['challengeID']);
	$startDate = checkInput($_GET['startDate']);
	$participantsIDs = checkInput($_GET['participantsIDs']);
	$participants = explode(",", $participantsIDs);
	$duration = intval($_GET['duration']);
	$inviterID = $userID;

	// build enddate
	$endDate = date('Y-m-d', strtotime($startDate)+($duration*24*60*60));
	
	// build sql query --> started_challenge
	$sql = "INSERT INTO started_challenge (`challengeID`, `startDate`, `endDate`) VALUES ('".$challengeID."', '".$startDate."', '".$endDate."')";
	
	// send query --> started_challenge
	$result = mysql_query($sql);
	
	$startedChallengeID = mysql_insert_id();

	// build sql query --> challenge_participants
	$error = false;	
	if (!empty($participants[0])) {
		foreach($participants as $key => $value) {
			if (!$error) {
				$sql = "INSERT INTO challenge_participants (`startedChallengeID`, `userID`, `isAccepted`, `duration`, `inviterID`) VALUES ('".$startedChallengeID."', '".$value."', '0', ".$duration.", ".$inviterID.")";
				if (!mysql_query($sql)) {
					$error = true;
				}
			}
		}
	}
	
	// own challenge data entry
	$sql = "INSERT INTO challenge_participants (`startedChallengeID`, `userID`, `isAccepted`, `duration`) VALUES ('".$startedChallengeID."', '".$userID."', '1', ".$duration.")";
	if (!mysql_query($sql)) {
		die("Error");
	}
				
	// select challenge data
	$sql = "SELECT * FROM started_challenge sc 
			INNER JOIN challenge c ON c.challengeID = sc.challengeID
			WHERE sc.startedChallengeID = ".$startedChallengeID;
	$queryResult = mysql_query($sql);
	$data = mysql_fetch_assoc($queryResult);
	
	// send sql query
	$result = array();
	if (!empty($data)) {
		$result['status'] = true;
		$result['challengeName'] = $data['challengeName'];
		$result['challengeDesc'] = $data['challengeDesc'];
		$result['usage'] = $data['usage'];
		$result['categoryID'] = $data['categoryID'];
		$result['startedChallengeID'] = $startedChallengeID;
		$result['todayDone'] = 0;
		$result['challengeNotStarted'] = 1;
		$result['daysLeft'] = round((strtotime($data['endDate']) - strtotime($data['startDate']))/86400, 0);
	}else{
		$result['status'] = false;
	}
	
	echo $_GET['jsoncallback'];
	echo '('.json_encode($result).');';
?>
