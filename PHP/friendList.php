<?php
	// include global
	require_once("global.php");
	$userid = intval($_GET['userID']);
		
	// friend request - both together
	$sqlfriends = "SELECT username,u.userID FROM friends f INNER JOIN user u ON (u.userID = f.userID OR u.userID = f.friendID)
WHERE (f.userID = ".$userid." OR f.friendID=".$userid.") AND f.isAccepted = 1 AND u.userID !=".$userid." ORDER BY username";

	// send friend sql query
	$resultfriends = mysql_query($sqlfriends);
	
	$friends = array();
	$friends['Alle Freunde']['username'] = 'Alle Freunde';
	$friends['Alle Freunde']['isFriend'] = 3;
	while ($row = mysql_fetch_assoc($resultfriends)) {
		$friends[$row['username']]['username'] = $row['username'];
		$friends[$row['username']]['isFriend'] = 1;
		$friends[$row['username']]['userID'] = $row['userID'];
	}
	
	
	// user I invitet - friend invite
	$sqluserinvite = "SELECT username,u.userID FROM friends f INNER JOIN user u ON f.friendID = u.userID WHERE f.userID = ".$userid."  AND f.isAccepted = 0 AND u.userID !=".$userid." ORDER BY username";
	
	$resultuserinvite = mysql_query($sqluserinvite);
	
	$userinvite = array();
	$userinvite['Alle Benutzer']['username'] = 'Alle Benutzer';
	$userinvite['Alle Benutzer']['isFriend'] = 3;
	while ($row = mysql_fetch_assoc($resultuserinvite)) {
		$userinvite[$row['username']]['username'] = $row['username'];
		$userinvite[$row['username']]['isFriend'] = 0;
		$userinvite[$row['username']]['userID'] = $row['userID'];
		$userinvite[$row['username']]['isInvited'] = 1;
		
		}
	// users who invitet me -- friend request
	$sqluserrequest = "SELECT username,u.userID FROM friends f INNER JOIN user u ON f.userID = u.userID WHERE f.friendID = ".$userid."  AND f.isAccepted = 0 AND u.userID !=".$userid." ORDER BY username";
	
	$resultuserrequest = mysql_query($sqluserrequest);
	
	$userrequests = array();
	$userrequests['Alle Benutzer']['username'] = 'Alle Benutzer';
	$userrequests['Alle Benutzer']['isFriend'] = 3;
	while ($row = mysql_fetch_assoc($resultuserrequest)) {
		$userrequests[$row['username']]['username'] = $row['username'];
		$userrequests[$row['username']]['isFriend'] = 0;
		$userrequests[$row['username']]['userID'] = $row['userID'];
		$userrequests[$row['username']]['isRequested'] = 1;
	}
	
	// all users(that means no friends, no friend invites and no friend requests)
	$sqluser = "SELECT username,u.userID from user u WHERE  u.userID != ".$userid." AND (username,u.userID) NOT IN (SELECT username,u.userID FROM friends f INNER JOIN user u ON (u.userID = f.userID OR u.userID = f.friendID)
WHERE f.userID = ".$userid." OR f.friendID= ".$userid.") ORDER BY username";
	
	// send user sql query
	$resultuser = mysql_query($sqluser);
	
	$user = array();
	$user['Alle Benutzer']['username'] = 'Alle Benutzer';
	$user['Alle Benutzer']['isFriend'] = 3;
	while ($row = mysql_fetch_assoc($resultuser)) {
		$user[$row['username']]['username'] = $row['username'];
		$user[$row['username']]['isFriend'] = 0;
		$user[$row['username']]['userID'] = $row['userID'];	
	}

	// merge two arrays
	$users = array_merge($user, $userinvite, $userrequests);
	$friends = array_merge($friends, $users);
	
	echo $_GET['jsoncallback'];
	echo '('.json_encode($friends).');';
?>