<?php
	error_reporting(E_ALL);
    // include global &  define constants
	require_once("global.php");
    //ini_set('display_errors', '1');
    define('THUMBNAIL_IMAGE_MAX_WIDTH', 2048);
    define('THUMBNAIL_IMAGE_MAX_HEIGHT', 2048);
    
	
	// assign variables
	$goal_title = checkInput($_POST['title']);
    $userid = intval($_REQUEST['userID']);
	
    // build sql query

	$sql = "INSERT INTO  goal (g_title , g_userID  )   VALUES ('".$goal_title."',  '".$userid."') ";
    
    //var_dump($_REQUEST);
    
    
    $result = array();
	if (mysql_query($sql)) {
		$result['status'] = true;
		$result['goal_title'] = $goal_title;
		$result['g_id'] = mysql_insert_id();
	}else{
		$result['status'] = false;
	}
	//print_r($result);
    
    //var_dump($_FILES);
        
    $image_extension = strtolower(substr(strrchr($_FILES['file']['name'], '.'), 1));
    $mime_mapping = array('png' => 'image/png', 'gif' => 'image/gif', 'jpg' => 'image/jpeg', 'jpeg' => 'image/jpeg');
    $info = getimagesize($_FILES['file']['tmp_name']);
    
    if (!$info) {
        die('not an image');
    }
    
    if ($info['mime'] != $mime_mapping[$image_extension]) {
        die('wrong extension given');
    }
    
    $uploaddir = './res/images/';
    
    $uploadfile = $uploaddir . md5("img_" . $result['g_id']);
    
    if (move_uploaded_file($_FILES['file']['tmp_name'], $result['g_id'] . ".jpg")) {
        //echo "File Upload Sucessfull";
        
        generate_image_thumbnail($result['g_id'] . ".jpg", $uploadfile . ".jpg");
        unlink($result['g_id'] . ".jpg");
        //$result['img_url'] = BACKEND_URL.'res/images/'.  md5("img_" . $result['g_id']) . ".jpg";
        //echo "File Moved Sucessfull";
        $result['URL'] = BACKEND_PATH."res/images/".md5("img_" . $result['g_id']).".jpg";
    } else {
        //echo "im 1 else";
         $result['status'] = false;   
        }
        

    
    
    
	echo $_GET['jsoncallback'];
	echo '('.json_encode($result).');';

    
    function generate_image_thumbnail($source_image_path, $thumbnail_image_path) {
        list($source_image_width, $source_image_height, $source_image_type) = getimagesize($source_image_path);
        switch ($source_image_type) {
            case IMAGETYPE_GIF :
                $source_gd_image = imagecreatefromgif($source_image_path);
                break;
            case IMAGETYPE_JPEG :
                $source_gd_image = imagecreatefromjpeg($source_image_path);
                break;
            case IMAGETYPE_PNG :
                $source_gd_image = imagecreatefrompng($source_image_path);
                break;
        }
        if ($source_gd_image === false) {
            return false;
        }
        $source_aspect_ratio = $source_image_width / $source_image_height;
        $thumbnail_aspect_ratio = THUMBNAIL_IMAGE_MAX_WIDTH / THUMBNAIL_IMAGE_MAX_HEIGHT;
        if ($source_image_width <= THUMBNAIL_IMAGE_MAX_WIDTH && $source_image_height <= THUMBNAIL_IMAGE_MAX_HEIGHT) {
            $thumbnail_image_width = $source_image_width;
            $thumbnail_image_height = $source_image_height;
        } elseif ($thumbnail_aspect_ratio > $source_aspect_ratio) {
            $thumbnail_image_width = (int)(THUMBNAIL_IMAGE_MAX_HEIGHT * $source_aspect_ratio);
            $thumbnail_image_height = THUMBNAIL_IMAGE_MAX_HEIGHT;
        } else {
            $thumbnail_image_width = THUMBNAIL_IMAGE_MAX_WIDTH;
            $thumbnail_image_height = (int)(THUMBNAIL_IMAGE_MAX_WIDTH / $source_aspect_ratio);
        }
        $thumbnail_gd_image = imagecreatetruecolor($thumbnail_image_width, $thumbnail_image_height);
        imagecopyresampled($thumbnail_gd_image, $source_gd_image, 0, 0, 0, 0, $thumbnail_image_width, $thumbnail_image_height, $source_image_width, $source_image_height);
        imagejpeg($thumbnail_gd_image, $thumbnail_image_path, 60);
        imagedestroy($source_gd_image);
        imagedestroy($thumbnail_gd_image);
        return true;
    }
    ?>
