<?php
	// include global
	require_once("global.php");

	// assign variables
	$userID = intval($_GET['userID']);

	// build sql query
	$sql = "SELECT * FROM challenge c
			INNER JOIN started_challenge sc ON sc.challengeID = c.challengeID
			INNER JOIN challenge_participants cp ON cp.startedChallengeID = sc.startedChallengeID
			WHERE cp.isFinished = 1
			AND cp.userID = ".$userID;

	// send sql query
	$result = mysql_query($sql);
	
	$challenges = array();
	while ($row = mysql_fetch_assoc($result)) {
		$challenges[] = $row;
	}
	
	echo $_GET['jsoncallback'];
	echo '('.json_encode($challenges).');';
?>